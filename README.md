# code-translate-server

#### 介绍
转译comfyui流程图并生成代码调用

#### 软件架构
自研可视化蓝图组件，转译兼容
对类蓝图节点编辑器有兴趣的，我开源了两个
```
https://github.com/1694439208/Avalonia_BluePrint
https://github.com/1694439208/BluePrint
```


#### 安装教程

>> 在ComfyUI目录创建一个extend目录，把项目文件都下载放进去


>> 然后修改ComfyUI目录下的server.py 找到下面这个代码 用这个替换了

```
@routes.get("/object_info")
async def get_object_info(request):
    out = {}
    for x in nodes.NODE_CLASS_MAPPINGS:
        out[x] = node_info(x)
    ret = web.json_response(out)
    current_script_path = os.path.dirname(os.path.abspath(__file__))
    print(current_script_path)
    with open('ComfyUI\\extend\\nodes.json', 'w') as f:
        json.dump(out, f)
    return ret
```

## 最后一步 复制一份run_nvidia_gpu.bat文件然后把里面的内容换成
```
.\python_embeded\python.exe -s ComfyUI\extend\newserver.py
pause
```
之后启动run_nvidia_gpu.bat之后再启动上面刚创建的bat即可使用蓝图转译生成python代码

