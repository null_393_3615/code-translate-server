

import asyncio
import base64
import json
import time
import traceback

from typing import Callable

import requests

from extend.nodeStruct import JoinType, NodeManager, NodeManagerServer, NodeToken, NodeType, argument, response_data


from extend.nodeServer import app





release = False
if release:
    from tool import img2img1, txt2img1

#from tool import img2img1
@NodeManagerServer.register(
    name = "设置字符串变量",
    title="设置字符串变量",
    describe="设置字符串变量",
    nodeType=NodeType.network,
    inputJoins={
        "执行1": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                #"defaultValue": [item["title"] for item in getmodels()],#模型列表
            }
        },
        "变量名": {
            "name": JoinType.Join_textbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"变量名",
                #"defaultValue": [item["title"] for item in getmodels()],#模型列表
            }
        },
        "变量值": {
            "name": JoinType.Join_textbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"变量值",
                #"defaultValue": [item["title"] for item in getmodels()],#模型列表
            }
        },
    },
    outputJoins={
        "接头2": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                "title":"执行",
            }
        },
    }
)
async def 设置字符串变量(arg:argument,result:response_data, func:Callable[[str,int, object], None]):
    变量名 = arg.get_argumentName("变量名","",str)
    变量值 = arg.get_argumentName("变量值","",str)
    VariableManager[变量名]=变量值
    result.set_nexts("接头2")
    return
VariableManager={}
@NodeManagerServer.register(
    name = "读取字符串变量",
    title="读取字符串变量",
    describe="读取字符串变量",
    nodeType=NodeType.network,
    inputJoins={
        "变量名": {
            "name": JoinType.Join_textbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"变量名",
                #"defaultValue": [item["title"] for item in getmodels()],#模型列表
            }
        },
    },
    outputJoins={
        "返回值": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Value,
            "data":{
                "title":"返回值",
            }
        },
    }
)
async def 读取字符串变量(arg:argument,result:response_data, func:Callable[[str,int, object], None]):
    变量名 = arg.get_argumentName("变量名","",str)
    result.set_results("返回值",VariableManager.get(变量名,""))
    return
@NodeManagerServer.register(
    name = "读取图片path",
    title="读取图片path",
    describe="读取图片path",
    nodeType=NodeType.network,
    inputJoins={
        "图片路径": {
            "name": JoinType.Join_textbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"图片路径(str)",
            }
        },
    },
    outputJoins={
        "image": {
            "name": JoinType.Join_img,
            "jointype": NodeToken.Value,
            "data":{
                "title":"图片base64(str)",
                "width":200,
                
            }
        },
    }
)
async def loadimg(arg:argument,result:response_data,  func:Callable[[str,int, object], None]):
    path = arg.get_argumentName("图片路径")
    print(path)
    if path != None:
        try:
            result.set_results("image",image_to_base64(path))
            return
        except Exception as e:
            result.set_error(f"{e}")
            return
    result.set_error("路径内容是空的")

@NodeManagerServer.register(
    name = "显示图片",
    title="显示图片",
    describe="显示图片",
    nodeType=NodeType.network,
    inputJoins={
        "接头": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                "title":"接头",
            }
        },
        "图片": {
            "name": JoinType.Join_img,
            "jointype": NodeToken.Value,
            "data":{
                #"width":300,
                #"title":"图片(str)",
            }
        },
    },
    outputJoins={
        "ret图片": {
            "name": JoinType.Join_img,
            "jointype": NodeToken.Value,
            "data":{
                "width":300.0,
                #"title":"图片(str)",
            }
        }
    }
)
async def showimg(arg:argument,result:response_data, func:Callable[[str,int, object], None]):
    path = arg.get_argumentName("图片")
    if path != None:
        try:
            result.set_results("ret图片",path)
            return
        except Exception as e:
            result.set_error(f"{e}")
            return
    result.set_error("图片是空的")


@NodeManagerServer.register(
    name = "文本节点",
    title="文本节点",
    describe="文本节点",
    nodeType=NodeType.network,
    inputJoins={
        "输入": {
            "name": JoinType.Join_textbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"文本",
                "width":200,
                "warp":10
            }
        },
    },
    outputJoins={
        "输出文本": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Value,
            "data":{
                "title":"文本",
            }
        },
    }
)
async def text(arg:argument,result:response_data, func:Callable[[str,int, object], None]):
    a = arg.get_argumentName("输入")
    result.set_results("输出文本",a)
    return



@NodeManagerServer.register(
    name = "保存原图",
    title="保存原图",
    describe="保存原图",
    nodeType=NodeType.network,
    inputJoins={
        "执行": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                "title":"执行",
            }
        },
        "图片": {
            "name": JoinType.Join_IntPtr,
            "jointype": NodeToken.Value,
            "connectionType":"bys", #图片类型
            "data":{
                "title":"图片数据",
            }
        },
        "路径": {
            "name": JoinType.Join_textbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"保存路径",
            }
        },
    },
    outputJoins={}
)
async def saveimg(arg:argument,result:response_data, func:Callable[[str,int, object], None]):
    try:
        img = arg.get_argumentName("图片","",str)
        savepath = arg.get_argumentName("路径","",str)
        image_bytes = base64.b64decode(img)    # 将 base64 编码的数据解码成 bytes
        with open(savepath, 'wb') as file:            # 使用 'wb' 模式打开一个新文件
            file.write(image_bytes)
        return
    except Exception as e:
        result.set_error(f"{e}")
        return
    return
def getmodels():
    if release :
        url = "http://127.0.0.1:7860/sdapi/v1/sd-models"
        response = requests.get(url)
        return json.loads(response.text)
    return [{"title":""}]
    
@NodeManagerServer.register(
    name = "设置模型",
    title="设置模型",
    describe="设置模型",
    nodeType=NodeType.network,
    inputJoins={
        "模型选择": {
            "name": JoinType.Join_dropdown,
            "jointype": NodeToken.Value,
            "data":{
                "defaultValue": [item["title"] for item in getmodels()],#模型列表
            }
        },

    },
    outputJoins={
        "输出文本": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Value,
            "data":{
                "title":"模型名字",
            }
        },
    }
)
async def selectModel(arg:argument,result:response_data, func:Callable[[str,int, object], None]):
    a = arg.get_argumentName("模型选择")
    result.set_results("输出文本",a)
    result.set_nexts("输出接头")
    return

@NodeManagerServer.register(
    name = "模拟耗时任务",
    title="模拟耗时任务",
    describe="模拟耗时任务",
    nodeType=NodeType.network,
    inputJoins={
        "接头": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                "title":"接头",
            }
        },
        "耗时": {
            "name": JoinType.Join_textbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"耗时秒(str)",
            }
        },
    },
    outputJoins={
        "输出接头": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                "title":"接头",
            }
        },
    }
)
async def sleep(arg:argument,result:response_data, func:Callable[[str,int, object], None]):
    try:
        size = arg.get_argumentName("耗时")
        print(size)
        if size != None:
            for i in range(int(size)):
                func("",i,None)
                await asyncio.sleep(1)
            result.set_nexts("输出接头")
        else:
            result.set_error("请输入“耗时”参数")
        return
    except Exception as e:
        result.set_error(f"{e}")
        return
@NodeManagerServer.register(
    name = "if",
    title="if",
    describe="if",
    nodeType=NodeType.network,
    inputJoins={
        "接头": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                "title":"接头",
            }
        },
        "条件": {
            "name": JoinType.Join_Checkbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"条件",
            }
        },
    },
    outputJoins={
        "true": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                "title":"true",
            }
        },
        "false": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                "title":"false",
            }
        },
    }
)
async def ifnode(arg:argument,result:response_data, func:Callable[[str,int, object], None]):
    try:
        size = arg.get_argumentName("条件",False,bool)
        print(size)
        if size == True:
            result.set_nexts("true")
        else:
            result.set_nexts("false")
        return
    except Exception as e:
        result.set_error(f"{e}")
        return

@NodeManagerServer.register(
    name = "文本比较",
    title="文本比较",
    describe="文本比较",
    nodeType=NodeType.network,
    inputJoins={
        "a": {
            "name": JoinType.Join_textbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"比较1",
            }
        },
        "b": {
            "name": JoinType.Join_textbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"比较2",
            }
        },
    },
    outputJoins={
        "bool": {
            "name": JoinType.Join_Checkbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"bool",
            }
        },
    }
)
async def 文本比较(arg:argument,result:response_data, func:Callable[[str,int, object], None]):
    try:
        a = arg.get_argumentName("a","",str)
        b = arg.get_argumentName("b","",str)
        if a == b:
            result.set_results("bool",True)
        else:
            result.set_results("bool",False)
        return
    except Exception as e:
        result.set_error(f"{e}")
        return

@NodeManagerServer.register(
    name = "文本存在",
    title="文本存在",
    describe="文本存在",
    nodeType=NodeType.network,
    inputJoins={
        "a": {
            "name": JoinType.Join_textbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"查找字符串",
            }
        },
        "b": {
            "name": JoinType.Join_textbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"被查找字符串",
            }
        },
    },
    outputJoins={
        "bool": {
            "name": JoinType.Join_Checkbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"bool",
            }
        },
    }
)
async def 文本存在(arg:argument,result:response_data, func:Callable[[str,int, object], None]):
    try:
        a = arg.get_argumentName("a","",str)
        b = arg.get_argumentName("b","",str)
        if a in b:
            result.set_results("bool",True)
        else:
            result.set_results("bool",False)
        return
    except Exception as e:
        result.set_error(f"{e}")
        return
    
async def image_to_base64(image_path):
    with open(image_path, "rb") as image_file:
        image_data = image_file.read()
        base64_data = base64.b64encode(image_data)
        base64_string = base64_data.decode("utf-8")
        return base64_string
@NodeManagerServer.register(
    name = "图生图",
    title="图生图",
    describe="图生图",
    nodeType=NodeType.network,
    inputJoins={
        "接头": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                "title":"接头",
            }
        },
        "tag": {
            "name": JoinType.Join_textbox,
            "jointype": NodeToken.Value,
            "data":{
                "title":"tag描述(str)",
            }
        },
        "图片": {
            "name": JoinType.Join_img,
            "jointype": NodeToken.Value,
            "data":{
                #"title":"图片路径(str)",
            }
        },
    },
    outputJoins={
        "接头": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                "title":"接头",
            }
        },
        "image": {
            "name": JoinType.Join_img,
            "jointype": NodeToken.Value,
            "data":{
                "title":"图片base64(str)",
                "width":200,
            }
        },
    }
)
async def img2img(arg:argument,result:response_data,  func:Callable[[str,int, object], None]):
    try:
        ret = img2img1(arg.get_argumentName("图片"),arg.get_argumentName("tag"))
        if type(ret) == Exception:
            raise ret
        result.set_results("接头",ret)
        return
    except Exception as e:
        result.set_error(f"{e}")
        return
    
@NodeManagerServer.register(
    name = "文生图",
    title="文生图",
    describe="文生图",
    nodeType=NodeType.network,
    inputJoins={
        "接头": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                "title":"接头",
            }
        },
        "模型": {
            "name": JoinType.Join_textbox,"jointype": NodeToken.Value,"data":{"title":"模型选择",}
        },
        "提示词": {
            "name": JoinType.Join_textbox,"jointype": NodeToken.Value,"data":{"title":"tag描述(str)",}
        },
        "正向提示词": {
            "name": JoinType.Join_textbox,"jointype": NodeToken.Value,"data":{"title":"正向tag描述",}
        },
        "反向提示词": {
            "name": JoinType.Join_textbox,"jointype": NodeToken.Value,"data":{"title":"反向tag描述",}
        },
        "生成宽度": {
            "name": JoinType.Join_textbox,"jointype": NodeToken.Value,"data":{"title":"图片宽度",}
        },
        "生成高度": {
            "name": JoinType.Join_textbox,"jointype": NodeToken.Value,"data":{"title":"图片高度",}
        },
        "重绘比例": {
            "name": JoinType.Join_textbox,"jointype": NodeToken.Value,"data":{"title":"重绘比例",}
        },
        "迭代步数": {
            "name": JoinType.Join_textbox,"jointype": NodeToken.Value,"data":{"title":"迭代步数",}
        },
        "引导系数": {
            "name": JoinType.Join_textbox,"jointype": NodeToken.Value,"data":{"title":"引导系数",}
        },
        "种子": {
            "name": JoinType.Join_textbox,"jointype": NodeToken.Value,"data":{"title":"种子",}
        },

    },
    outputJoins={
        "接头": {
            "name": JoinType.Join_str,
            "jointype": NodeToken.Call,
            "data":{
                "title":"接头",
            }
        },
        "image": {
            "name": JoinType.Join_img,
            "jointype": NodeToken.Value,
            "data":{
                "title":"图片base64(str)",
                "width":300,
            }
        },
    }
)
async def txt2img(arg:argument,result:response_data,  func:Callable[[str,int, object], None]):
    try:
        模型 = arg.get_argumentName("模型","")
        提示词 = arg.get_argumentName("提示词","")
        正向提示词 = arg.get_argumentName("正向提示词","")
        反向提示词 = arg.get_argumentName("反向提示词","")
        生成宽度 = arg.get_argumentName("生成宽度",512.0,float)
        生成高度 = arg.get_argumentName("生成高度",512.0,float)
        重绘比例 = arg.get_argumentName("重绘比例",0.7,float)
        迭代步数 = arg.get_argumentName("迭代步数",20.0,float)
        引导系数 = arg.get_argumentName("引导系数",8.0,float)
        种子 = arg.get_argumentName("种子",-1)
        print(模型,提示词,正向提示词,反向提示词,
                       生成宽度,生成高度,重绘比例,
                       迭代步数,引导系数,种子)
        ret = txt2img1(模型,提示词,正向提示词,反向提示词,
                       生成宽度,生成高度,重绘比例,
                       迭代步数,引导系数,种子)
        if type(ret) == Exception:
            raise ret
        print("ret:",ret)
        result.set_results("image",ret)
        return
    except Exception as e:
        print("txt2img")
        print(e)
        result.set_error(f"{e}")
        traceback.print_exc()
        return


NodeManagerServer.serverInfo["version"] = "0.1.3"

from aiohttp import web
#   启动web服务
web.run_app(app, host='0.0.0.0', port=5000)

