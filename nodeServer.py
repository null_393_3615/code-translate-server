from asyncio import QueueEmpty
import asyncio
import datetime
import json
import logging
import time
import traceback
import uuid
from aiohttp import request, web
import aiohttp_cors
from aiohttp_session import get_session, session_middleware, setup
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from aiohttp_sse import sse_response


route = web.RouteTableDef()# 作者：郑健阳 https://www.bilibili.com/read/cv9457917/ 出处：bilibili

from extend.nodeStruct import setTaskProgressData,NodeManagerServer, TaskProgressDataManager, argument, custom_serializer, response_data

# 配置日志记录器
logging.basicConfig(level=logging.DEBUG)  # 设置日志级别为 DEBUG

#任务变量生命周期管理器
variableLifeManager:dict[str,dict[str,object]] = {}

@route.get('/taskConnect')
async def taskConnect(request: web.Request):
    logging.debug("收到请求taskConnect", request.query.get('taskId'))
    session = await get_session(request)
    session['taskId'] = request.query.get('taskId')

    async with sse_response(request) as resp:
        try:
            while True:
                data = 'Server Time : {}'.format(datetime.datetime.now())
                await resp.send(data)
                await asyncio.sleep(1)
        except Exception as e:
            taskId = session.get('taskId')
            del variableLifeManager[taskId]
            logging.debug(f"客户端执行完毕，任务id： {taskId}")
            # 对特定用户断开进行处理...
       


@route.get('/nodeserver')               
async def nodeservear(request: web.Request):
    return web.json_response(NodeManagerServer.serverInfo)


@route.get('/getTaskProgressData')               
async def getTaskProgressData(request: web.Request):
    taskId = request.query.get('taskId')
    tasknode_id = request.query.get('taskNodeId')
    #print(request.args)
    if tasknode_id != None and taskId != None:
        taskqueue = TaskProgressDataManager.get(taskId)
        if taskqueue!=None:
            queue = taskqueue.get(tasknode_id)
            if queue!=None and not queue.empty():
                ret = queue.get_nowait() 
                logging.debug(f"getTaskProgressData:{ret}")
                return web.json_response(ret)
    return web.json_response({})


import nodes

from comfy import utils


async def findNode(name,**args):
    node = nodes.NODE_CLASS_MAPPINGS.get(name,None)
    if node:
        cls = node()
        func = getattr(cls, node.FUNCTION)
        return await asyncio.to_thread(func,args)
    return None

@route.post('/execute') #节点执行
async def execute(request: web.Request):
    data = await request.json()
    text = await request.text()
    logging.debug(f"execute-*text:{data}")
    name = data["node"]                 #节点的name，


    node = nodes.NODE_CLASS_MAPPINGS.get(name,None)
    logging.debug(f"node.FUNCTION:{node.FUNCTION}")
    if node:
        cls = node()
        func = getattr(cls, node.FUNCTION)
        INPUT_TYPES = getattr(cls, "INPUT_TYPES")
        ret = response_data()
        try:
            arg = argument(data)
            args = {}
            arg_names = INPUT_TYPES()["required"]
            for item in arg_names:
                temp_arg = arg_names[item]
                if len(temp_arg) > 0:
                    if isinstance(temp_arg[0],str):
                        if temp_arg[0] == "INT":
                            args.update({
                                item:arg.get_argumentName(item,0,int)
                            })
                        elif temp_arg[0] == "FLOAT":
                            args.update({
                                item:arg.get_argumentName(item,0.0,float)
                            })
                        elif temp_arg[0] == "STRING":
                            args.update({
                                item:arg.get_argumentName(item,"",str)
                            })
                        else:
                            #不是基础类型要从管理器取数据
                            logging.error(f"*****variableLifeManager:{variableLifeManager}")
                            if arg.taskId in variableLifeManager:
                                varmap = variableLifeManager.get(arg.taskId)
                                varname = arg.get_argumentName(item,"",str)
                                logging.error(f"*****varname:{varname}")
                                args.update({
                                    item:varmap.get(varname,None)
                                })
                            else:
                                args.update({
                                    item:None
                                })
                            logging.error(f"类型错误：{temp_arg[0]}-type:{type(temp_arg[0])},arg:{args}")
                    elif isinstance(temp_arg[0],list):
                        args.update({
                            item:arg.get_argumentName(item,0,str)
                        })
                    else:
                        #不是基础类型要从管理器取数据
                        if arg.taskId in variableLifeManager:
                            varmap = variableLifeManager.get(arg.taskId)
                            varname = arg.get_argumentName(item,"",str)
                            args.update({
                                item:varmap.get(varname,None)
                            })
                else:
                    logging.error(f"参数错误：{item}")
                    args.update({
                        item:args.append(None)
                    })
                    
            print("*"*50)
            print(args)

            #执行前设置调用回调
            def hook(value, total, preview_image):
                logging.debug(f"进度:{total}")
                setTaskProgressData(arg.taskId,arg.taskNodeId,"",total,value)
            utils.set_progress_bar_global_hook(hook)
            
            #wrapper = lambda: test(**args)
            ret_tuple = await asyncio.to_thread(func,**args)
            #ret_tuple = func(**args)
            logging.error(f"*****执行返回ret_tuple:{ret_tuple}")

            #traceback.print_stack()
            for i in range(len(node.RETURN_TYPES)):
                name = node.RETURN_TYPES[i]
                logging.error(f"*****返回参数名称:{name}")
                if name in ["INT","FLOAT","STRING","IMG","str"]:
                    ret.set_results(name,ret_tuple[i])
                else:
                    #如果都没命中，那就需要引用
                    pointer = str(uuid.uuid4())
                    logging.debug(f"******name:{pointer}")
                    ret.set_results(name,pointer)
                    if arg.taskId in variableLifeManager:
                        varmap = variableLifeManager.get(arg.taskId)
                        varmap.update({
                            pointer: ret_tuple[i]
                        })
                    else:
                        variableLifeManager.update({
                            arg.taskId:{
                                pointer : ret_tuple[i]
                            }
                        })
                
            #
            for i in arg.get_nexts():
                ret.set_nexts(i)

            #await node[node.FUNCTION](arg,ret,lambda joinname,progressValue,value: setTaskProgressData(arg.taskId,arg.taskNodeId,joinname,value,progressValue))
        except Exception as e:
            logging.error(f"方法名:{node.FUNCTION},执行错误：{e}")
            ret.error = f'{e}'
    print({
        "context":ret.context,
        "result":{                        #返回数据
            "nexts":ret.nexts,
            "results":ret.results,
            "error":ret.error,                   #节点执行错误就设置错误文本
        }
    })
    return web.json_response({
        "context":ret.context,
        "result":{                        #返回数据
            "nexts":ret.nexts,
            "results":ret.results,
            "error":ret.error,                   #节点执行错误就设置错误文本
        }
    })


@route.get('/ping')                       #判断服务器状态
async def ping(request: web.Request):
    return web.json_response({
        "code":"200"                      #200成功，目前只判断这一个code  不成功就随便值
    })



def getJoinData(name,value):
    if name == "control_after_generate":
        return {
            name:{
                "name":"Join_dropdown",
                "jointype":"Value",
                "data": {
                    "title": name,
                    "width":260.0,
                    "defaultValue":["fixed", "increment", "decrement", "randomize"]
                }
            }
        }
    join_name = "Join_test"
    connectionType = ""
    data = {"title": name}
    if len(value) == 0:
        return {
        "name":join_name,
        "jointype":"Value",
        "data": {
            "title": name
        }.update(data)
    }
    
    item = value[0]
    if isinstance(item,list):
        join_name = "Join_dropdown"
        data["width"]=260.0
        data["defaultValue"]=item
    if isinstance(item,str):
        if item in ["INT","FLOAT","STRING"]:
            join_name = "Join_textbox"
            connectionType=item
        elif item == "IMG":
            join_name = "Join_img"
            data.update({"width":280.0})
        else:
            join_name = "Join_IntPtr"
            data.update({"title":name})
    ret = {
        "name":join_name,
        "jointype":"Value",
        "data": data
    }
    if connectionType != "":
        ret.update({"connectionType":connectionType})
    return {
        name:ret
    }
@route.get('/getNodeList')                #取节点列表          
async def getNodeList(request: web.Request):
    '''result = [{k: v for k, v in node.items() if k != 'func'} for node in NodeManagerServer.registered_nodes.values()]
    print(result)
    return web.json_response(result, dumps=lambda obj: json.dumps(obj, default=custom_serializer))'''
    
    with open('C:\\Users\\pc\\Downloads\\ComfyUI_windows_portable_nvidia_cu118_or_cpu\\ComfyUI_windows_portable\\ComfyUI\\extend\\nodes.json', 'r', encoding='utf-8') as f:
        data = json.load(f)
    
    
    NodeList = []
    for key,item in data.items():
        input = item["input"]["required"]

        def sort_by_property(item):
            value = input[item]
            if len(value) == 1:
                if isinstance(value[0], list):
                    return 1
                return 0
            return 1
        

        sorted_list = sorted(input, key=sort_by_property)
        if "seed" in sorted_list:
            index = sorted_list.index("seed")
            sorted_list.insert(index + 1, "control_after_generate")
        print("sorted_list",sorted_list)

        output = item["output"]
        outputname = item["output_name"]
        inputJoins = {}
        ouputJoins = {}
        if item["name"] in ["SaveImage","ShowImage"]:
            inputJoins.update({
                "输入接头": {
                    "name": "Join_test",
                    "jointype":"Call",
                    "data":{
                        "title":"流程"
                    }                      
                }
            })
            ouputJoins.update({
                "输出接头": {
                    "name": "Join_test",
                    "jointype":"Call",
                    "data":{
                        "title":"流程"
                    }                      
                }
            })
        for name in sorted_list:
            value = input.get(name,None)
            inputJoins.update(getJoinData(name,value))
        for name in zip(outputname,output):
            ouputJoins.update(getJoinData(name[0],[name[1]]))
        
        
        NodeList.append({
            "name":item["name"],
            "nodeType":"network",
            "describe":item["description"],
            "title":item["display_name"],
            "inputJoins":inputJoins,
            "ouputJoins":ouputJoins
        })


    return web.json_response(NodeList)

app = web.Application()

app.router.add_routes(route)

# 注册全局CORS策略，允许所有源站点通过所有HTTP方法访问所有路由：
cors = aiohttp_cors.setup(app, defaults={
    "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
            allow_methods="*",
    )
})
# 为所有路由添加CORS支持：
for route in list(app.router.routes()):
    cors.add(route)

# 设置会话中间件
secret_key = b'70f05acd596cc882bf08e47a83d111d2'
middleware = session_middleware(EncryptedCookieStorage(secret_key))
app.middlewares.append(middleware)
setup(app, EncryptedCookieStorage(secret_key))



