from asyncio import Queue, QueueEmpty
import json
import time
from typing import Dict, Union
from enum import Enum

class NodeType(Enum):
    network="network"
    native="native"
class NodeToken(Enum):                  
    Expression="Expression"            #这不能用，这是给解释器用的
    ExpressionValue="ExpressionValue"  #这不能用，这是给解释器用的
    Call="Call"                        #如果设置那么接头只能用来连执行线
    Value="Value"                      #如果设置那么接头只能用来当参数线
    CallValue="CallValue"              #这个接头作用是既可以连接执行线也可以传递参数，一般用来控制流处理，不建议用
    ObjectValue="ObjectValue"          #这个是接头本身就是数据，flutter下算是废弃了，
    Non="None"                         #默认啥也不干
class JoinType(Enum):
    Join_str="Join_test"               #显示一个文本标签，类型string
    Join_img="Join_img"                #显示一张图片，    类型string 但是base64的图片
    Join_button="Join_button"          #一个按钮         类型but ，but是逻辑类型，默认null  这个用来测试的
    Join_textbox="Join_textbox"        #一个输入框       类型string
    Join_Checkbox="Join_Checkbox"      #一个选择框       类型bool
    Join_dropdown="Join_dropdown"      #一个下拉框       类型string 可以传默认值 "datalist":["sd古风","sd科幻","sd二刺螈"],
    Join_AddJoin="Join_AddJoin"      
    Join_IntPtr="Join_IntPtr"      


class NodeManager:
    registered_nodes = {}
    def __init__(self,name,describe, version, enable = True,):
        self.serverInfo ={
            "version":version,              #版本     
            "enable":enable,                  #是否启用     
            "name":name,         #名称
            "describe":describe            #描述
        }
   
    @classmethod
    def register(cls, name, describe, title, nodeType, inputJoins, outputJoins):
        def decorator(func):
            node_info = {
                'name': name,
                'describe': describe,
                'title': title,
                'nodeType': nodeType,
                'inputJoins': inputJoins,
                'ouputJoins': outputJoins,
                "ipAddress": "",
                "uuId": "",
                'func': func
            }
            cls.registered_nodes[name] = node_info
            return func
        return decorator

class argument:
    def __init__(self,data):
        self.name = data["node"]                 #节点的name，
        self.taskNodeId = data["taskNodeId"]       #当前任务id，此节点执行生命期间有效-用来检测处理任务上下文
        self.taskId = data["taskId"]               #当前节点任务id，此节点执行生命期间有效-用来检测进度回调
        self.context = data["context"]             #执行参数上下文  是个map<string,object>对象  object就是一些基础类型 除了byte[] 这个要转为base64，因为json不支持
        self.arguments = data["arguments"]         #节点参数 也就是当前节点的接头数量的value
        self.nexts = data["nexts"]                 #下一个流程线名称数组
        self.clientID = data["clientID"]           #客户端id，跟随客户端进程的生命周期
    def get_name(self):
        return self.name
    def get_nexts(self) -> list[str]:
        return self.nexts
    def get_clientID(self):
        return self.clientID
    def get_context(self,key):
        try:
            item = self.context[key]
            return item
        except IndexError:
            return None
    def get_argumentIndex(self,index):
        try:
            item = self.arguments[index]["value"]
            return item
        except IndexError as e:
            print(e)
            return None
    def get_argumentName(self,name,defvalue = None,target_type = str):
        try:
            values = [item['value'] for item in self.arguments if item['name'] == name]
            ret = next(iter(values),defvalue)
            if ret == None:
                return defvalue
            return target_type(ret)
        except IndexError as e:
            print(e)
            return defvalue
    
class response_data:
    def __init__(self):
        self.name = ""                 #节点的name，
        self.context = {}             #执行参数上下文  是个map<string,object>对象  object就是一些基础类型 除了byte[] 这个要转为base64，因为json不支持
        self.results = {}         #节点参数 也就是当前节点的接头数量的value
        self.nexts = []                 #下一个流程线数量
        self.error = ""           #客户端id，跟随客户端进程的生命周期
    def set_context(self,key,value):
        self.context[key] = value
    def set_results(self,name:str,value):
        self.results[name] = value
    def set_nexts(self,name:str):
        self.nexts.append(name)
    def set_error(self,value):
        self.error = value

def custom_serializer(obj):
        if isinstance(obj, Enum):
            return obj.value
        raise TypeError("Object of type {} is not JSON serializable".format(type(obj).__name__))

NodeManagerServer = NodeManager("ai节点服务","用来提供sd相关的api","0.1.1")
#节点执行进度管理
TaskProgressDataManager:Dict[str, Dict[str, Queue]] = {}




def claerTaskProgressData(taskId,tasknode_id):
    taskqueue = TaskProgressDataManager.get(taskId)
    if taskqueue!=None:
        if tasknode_id in taskqueue:
            taskqueue.pop(tasknode_id)

def setTaskProgressData(taskId,tasknode_id,join_name,value,progressValue):
    if taskId in TaskProgressDataManager:
        taskqueue = TaskProgressDataManager.get(taskId)
        if taskqueue!=None:
            if tasknode_id in taskqueue:
                queue = taskqueue.get(tasknode_id)
                if queue!=None:
                    queue.put_nowait({
                        "progressValueTracker":progressValue,
                        "progressValue":{
                            join_name:value
                        }
                    })
    else:
        q = Queue()
        q.put_nowait({
                "progressValueTracker":progressValue,
                "progressValue":{
                    join_name:value
                }
            })
        TaskProgressDataManager[taskId] = {
            tasknode_id:q
        }




